let quiz = document.querySelector('#quiz');
let ques = document.querySelector('#question');
let result = document.querySelector('#result');
let nextButton = document.querySelector('#next');
let Q = document.querySelector('#quit');


// import la variable let questions from questionData
let tques = questions.length;
let score = 0;
let quesindex = 0;

// you can quit the game after starting 
// when you click on the button it will show you your current score depending on 
// the questions you have answered

function quit() {
    quiz.style.display = 'none';
    result.style.display = '';
    let f = score / tques;
    result.textContent = "SCORE =" + parseInt(f * 100);
    Q.style.display = "none";

}


// a function that displays the questions 
function give_ques(quesindex) {
    setTimeout(function(){ 
        ques.textContent = quesindex + 1 + "." + questions[quesindex].question;
        //selectionner la div reponses et la vider 
        
        let reponses = document.querySelector('#reponse');
        reponses.innerHTML = '';
    

        //faire une boucle classique sur les answer de la question actuelle

        //Faire que à chaque tour de boucle, ça crée avec createElement
        // opt1.textContent = questions[quesindex].answer[0];
        // opt2.textContent = questions[quesindex].answer[1];

        let answer = questions[quesindex].answer;

        for (let index = 0; index < answer.length; index++) {


            let label = document.createElement('label');
            label.classList.add('option');
            reponses.appendChild(label);

            let input = document.createElement('input');
            input.type = 'radio';
            input.name = 'option';
            input.value = index;
            label.appendChild(input);

            let span = document.createElement('span');
            span.textContent = answer [index];
            label.appendChild(span);

        }
    },1000);
    return ;
}

give_ques(0);
// a function that contains my questions : I have to choose an option in order to get to the next question 
// else an alert will op up to tell me that unless I choose an option I can't move to the next question
// + in each question a score will be added if the ans is valid score gets 1 
// if the ans is not valid the score gets nothing 

// condition of the end of game 

// changing the style of the index page to show only the div that contains the final score 

function nextQuestion() {
    let selectedAnswer = document.querySelector('input[type=radio]:checked');
    let selectedSpan = selectedAnswer.parentNode;
    if (!selectedAnswer) { alert("PLEASE SELECT AN OPTION TO GET TO THE NEXT QUESTION    "); return; }
//essayer de faire une méthode pour selectionner la bonne reponse plus le score/ fait avec la class Questions method
    

    
if (selectedAnswer.value == questions[quesindex].goodAnswer){
    score = score + 1; 
     selectedSpan.style.backgroundColor = 'green';
     

    }else{
        selectedSpan.style.backgroundColor = 'red';    }



    selectedAnswer.checked = false;
    quesindex++;

    if (quesindex == tques - 1){
        nextButton.textContent = "Finished";
    }
        
    let scoreFinal = score / tques;
    if (quesindex == tques) {
        Q.style.display = 'none';
        quiz.style.display = 'none';
        result.style.display = '';
        result.textContent = "Your Score is : " + parseInt(scoreFinal * 100);
        
        let para = document.createElement('p');
        para.innerHTML = `<h2>The Answers:</h2> <p>1.The earth is composed of 70% of water</p>
         <p>2.The vegetable waste  are recycable</p>
         <p>3.The 3 pilars of sustaible develpment are economic, social and environemental </p> 
         <p>4.A chewing-gum takes five years to be decomposed </p> 
        <p>5. True the hand scooter are less polluting than a bicycle</p> 
        <p>6. A cigarette takes 2 years to be decomposed</p>`;
        result.appendChild(para);
        
        let para2 = document.createElement('p');
        para2.innerHTML = 'If you want to see more => ';
        result.appendChild(para2)

        let link1 = document.createElement('a');
        link1.href = 'https://www.ecology.com/';
        link1.innerHTML = 'Ecology';
        para2.appendChild(link1);


        return;
    }

    give_ques(quesindex);
   
}
