/**
 * 1. Faire la classe et assigner question/réponses possibles/bonne réponse
 * 2. Faire une méthode de réponse à la question
 * 
 */

class Questions {
    /**
     * 
     * @param {*} question 
     * @param {*} answer 
     * @param {*} goodAnswer
     * @param {Integer} scorePoint
     */
    constructor (question, answer, goodAnswer, scorePoint){
        this.question = question;
        this.answer = answer;
        this.goodAnswer = goodAnswer;
        this.scorePoint = scorePoint;
    }
    
    
    // score(scoreFinal){        
    //     return scoreFinal + 1;
    //     //return parseInt(scoreFinal * 100);
    // }
    
    guess(value){

        if (value == this.goodAnswer){
            return true;
        }else {
            return false;
        }        
    }
}