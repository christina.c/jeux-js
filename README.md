# Jeu JavaScript

 

I want to make a JavaScript game, quiz-like, grouping three main themes:



- Planet Earth
- Sustainable development
- Recycling



## Description 



The player plays alone. He  will have to answer to six questions at all, one on each topic .

I suggested 2 or 4 answers and the player has to make one choice. If the answer is right, the green color appears and if it's wrong it becomes red. At the end, we can see the answers and if you want you can click the link to know more.



## Why this game



I would like to make a game that affects the biggest number of people and that allows to learn on each topic. And at the same time to make people aware that even we are different we can do better choices.



## User Stories



- As a player I wish to have fun and find an interest in the game.
- As a teacher I want to be able to assess the skills of my students.
- As a recruiter I want to be able to see the language addressed and understood by the learner.



## UML

<img src ='image/class.png'>



<img src = 'image/useCase.png'>



## Models



Here is my model : 



<img src = 'image/20190905_152722.jpg'></img>



<img src = 'image/20190905_152743.jpg'></img>



And here's a screenshot of my app :

<img src ='image/jeu.png'></img> 



<img src='image/end.png'></img>



## Milestone

- My first milestone is "launch project game" to start the search
- For the second I made the JavaScript 
- After I made the design of the game 
- And for the end I changed the design of my game 



## Code explanation

I would like to talk to you about my code, on this code we see the creation on JavaScript about different element.

We see the creation about a paragraph and a link.

```javascript
   let para = document.createElement('p');
        para.innerHTML = `<h2>The Answers:</h2> <p>1.The earth is composed of 70% of water</p>
         <p>2.The vegetable waste  are recycable</p>
         <p>3.The 3 pilars of sustaible develpment are economic, social and environemental</p> 
         <p>4.A chewing-gum takes five years to be decomposed </p> 
        <p>5. True the hand scooter are less polluting than a bicycle</p> 
        <p>6. A cigarette takes 2 years to be decomposed</p>`;
        result.appendChild(para);
        
        let para2 = document.createElement('p');
        para2.innerHTML = 'If you want to see more => ';
        result.appendChild(para2)

        let link1 = document.createElement('a');
        link1.href = 'https://www.ecology.com/';
        link1.innerHTML = 'Ecology';
        para2.appendChild(link
```



Now on this code we see the creation of a label, an input and a span for the answers.  



```javascript
 
 			let label = document.createElement('label');
            label.classList.add('option');
            reponses.appendChild(label);

            let input = document.createElement('input');
            input.type = 'radio';
            input.name = 'option';
            input.value = index;
            label.appendChild(input);

            let span = document.createElement('span');
            span.textContent = answer [index];
            label.appendChild(span);

```